package logger
import (
  "strings"

  "github.com/spf13/viper"
  goLogger "github.com/apsdehal/go-logger"
)

func GetLogLevel() goLogger.LogLevel {
  logLevel := strings.ToUpper(viper.GetString("LOG_LEVEL"))
  switch logLevel {
  case "CRITICAL":
    return goLogger.CriticalLevel
  case "ERROR":
    return goLogger.ErrorLevel
  case "WARNING":
    return goLogger.WarningLevel
  case "NOTICE":
    return goLogger.NoticeLevel
  case "INFO":
    return goLogger.InfoLevel
  case "DEBUG":
    return goLogger.DebugLevel
  default:
    Log.Fatalf("Unkown LOG_LEVEL %v, expected one of: CRITICAL|ERROR|WARN|NOTICE|INFO|DEBUG", logLevel)
  }
  return goLogger.InfoLevel
}
