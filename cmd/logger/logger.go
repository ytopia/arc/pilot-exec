package logger

import (
	"log"

  "github.com/spf13/viper"
  goLogger "github.com/apsdehal/go-logger"

)

var ModuleName = "pilot-exec"

var Log *goLogger.Logger
func Load(){
  logColor := viper.GetBool("LOG_COLOR")
  var logColorState int
  if logColor {
  logColorState = 1
  } else {
    logColorState = 0
  }
  logger, err := goLogger.New(ModuleName, logColorState)
  if err != nil {
    log.Fatal(err)
  }
  Log = logger

  logger.SetFormat("%{module} | %{level} | %{message}")

  logLevel := GetLogLevel()
  logger.SetLogLevel(logLevel)
}
