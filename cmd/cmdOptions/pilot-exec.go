package cmdOptions

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var consulHost string

func AddFlagConsulHost(cmd *cobra.Command) {
	cmd.Flags().StringVar(&consulHost, "consul-host", "consul", "external consul host")
	viper.BindPFlag("CONSUL_HOST", cmd.Flags().Lookup("consul-host"))
}

var consulPort string

func AddFlagConsulPort(cmd *cobra.Command) {
	cmd.Flags().StringVar(&consulPort, "consul-port", "8500", "external consul http api port")
	viper.BindPFlag("CONSUL_PORT", cmd.Flags().Lookup("consul-port"))
}

var consulHttps bool

func AddFlagConsulHttps(cmd *cobra.Command) {
	cmd.Flags().BoolVar(&consulHttps, "consul-https", false, "external consul http api enable https")
	viper.BindPFlag("CONSUL_HTTPS", cmd.Flags().Lookup("consul-https"))
}


var dependencies string

func AddFlagDependencies(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&dependencies, "dependencies", "d", "[]", "dependencies in Json5 array format list of 'service-name' or object {type: script|service|network|http|scripts-dir, target: []|string }")
	viper.BindPFlag("DEPENDENCIES", cmd.Flags().Lookup("dependencies"))
}

var dependenciesRetry string

func AddFlagRetry(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&dependenciesRetry, "retry", "r", "true", "default retry for dependencies as number or true for no limit")
	viper.BindPFlag("RETRY", cmd.Flags().Lookup("retry"))
}

var dependenciesRetryTimeout string

func AddFlagTimeout(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&dependenciesRetryTimeout, "timeout", "t", "20m", "default retry timeout for dependencies")
	viper.BindPFlag("TIMEOUT", cmd.Flags().Lookup("timeout"))
}

var logLevel string

func AddFlagLogLevel(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&logLevel, "log-level", "l", "INFO", "log level CRITICAL|ERROR|WARN|NOTICE|INFO|DEBUG")
	viper.BindPFlag("LOG_LEVEL", cmd.Flags().Lookup("log-level"))
}

var logColor bool

func AddFlagLogColor(cmd *cobra.Command) {
	cmd.Flags().BoolVarP(&logColor, "log-color", "x", false, "enable log color")
	viper.BindPFlag("LOG_COLOR", cmd.Flags().Lookup("log-color"))
}
