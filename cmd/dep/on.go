package dep

import (
	"sync"
	"time"

  "gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

func On(dependenciesJSON string) (bool, error) {
	dependencies := NormalizeDependencies(dependenciesJSON)

	var ok bool = true
	var err error

  logger.Log.InfoF("dependencies: %v", dependenciesJSON)

	dependenciesLength := len(dependencies)
	if dependenciesLength > 0 {

		var wg sync.WaitGroup
		wg.Add(dependenciesLength)

		quit := make(chan struct{})
		var quitOnce sync.Once
		quitOnceFunc := func() {
			close(quit)
		}

		startedTime := time.Now()
		for _, dependency := range dependencies {
			go func(dependency Dependency) {
				defer wg.Done()
				resolved, failed := WaitForDependency(dependency, startedTime, quit)
				if !resolved {
					ok = false
					if failed != nil {
						err = failed
					}
					quitOnce.Do(quitOnceFunc)
				}
			}(dependency)
		}
		wg.Wait()
	}

  if ok {
    logger.Log.Notice("dependencies ready")
  } else {
    logger.Log.Error("dependencies failed")
  }

	return ok, err
}
