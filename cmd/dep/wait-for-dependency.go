package dep

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

func WaitForDependency(dependency Dependency, startedTime time.Time, quit chan struct{}) (bool, error) {
	Type := dependency.Type
	Target := dependency.Target
	Retry := dependency.Retry
	RetryTimeout := dependency.RetryTimeout

	var RetryTypeInt bool = false
	var RetryTypeBool bool = false
	switch Retry.(type) {
	case int:
		RetryTypeInt = true
	case bool:
		RetryTypeBool = true
	}

	var RetryTimeoutTypeDuration bool = false
	var timeoutTime time.Time
	switch RetryTimeout.(type) {
	case time.Duration:
		RetryTimeoutTypeDuration = true
		var RetryTimeoutDuration time.Duration = RetryTimeout.(time.Duration)
		timeoutTime = startedTime.Add(RetryTimeoutDuration)
	}

	try := 0
	for {
		select {
		case <-quit:
			return false, nil
		default:
      logger.Log.DebugF("%s %v: resolving try: %v...", Type, Target, try+1)
			var resolved bool
			switch Type {
			case "service":
				service := Target[0]
				resolved = CheckService(service)
			case "network":
				var network []string
				if len(Target) == 1 {
					network = strings.Split(Target[0], ":")
				} else {
					network = Target[0:2]
				}
				host := network[0]
				port := network[1]
				resolved = CheckNetwork(host, port)
			case "script":
				resolved = CheckScript(Target)
      case "scripts-dir":
        resolved = CheckScriptsDir(Target[0])
			case "http":
				resolved = CheckHttp(Target)
			}

			if resolved {
				logger.Log.NoticeF("%s %v: ready", Type, Target)
				return true, nil
			} else if (RetryTypeInt && try >= Retry.(int)) || (RetryTypeBool && !Retry.(bool)) {
        logger.Log.ErrorF("%s %v: failed retry=%v", Type, Target, Retry)
				return false, fmt.Errorf("%s %v: failed retry=%v", Type, Target, Retry)
			} else if RetryTimeoutTypeDuration && time.Now().After(timeoutTime) {
				logger.Log.ErrorF("%s %v: failed retry-timeout=%v", Type, Target, RetryTimeout)
				return false, fmt.Errorf("%s %v: failed retry-timeout=%v", Type, Target, RetryTimeout)
			} else {
        if try == 0 {
        	logger.Log.InfoF("%s %v: waiting...", Type, Target)
        }
				time.Sleep(1 * time.Second)
				try++
			}
		}
	}

}
