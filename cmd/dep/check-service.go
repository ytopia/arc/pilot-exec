package dep

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/config"
)

func CheckService(service string) bool {

  Config := &config.Config

	consulHost := Config.CONSUL_HOST
	consulPort := Config.CONSUL_PORT
  var consulProto string
	if Config.CONSUL_HTTPS {
		consulProto = "https"
	} else {
		consulProto = "http"
	}

	consulAddr := fmt.Sprintf("%s:%s", consulHost, consulPort)

	res, err := http.Get(consulProto + "://" + consulAddr + "/v1/health/service/" + service + "?passing")
	if err != nil {
		return false
	}
	defer res.Body.Close()

	var servicePassing interface{}
	json.NewDecoder(res.Body).Decode(&servicePassing)
	servicePassingArray := servicePassing.([]interface{})
	if len(servicePassingArray) > 0 {
		return true
	} else {
		return false
	}

}
