package dep

import (
// "time"
)

type Dependency struct {
	Type         string
	Target       []string
	Retry        interface{}
	RetryTimeout interface{}
}
