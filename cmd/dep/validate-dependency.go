package dep

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func ValidateDependencyType(Type string) bool {
	switch Type {
	case
		"service",
		"network",
		"script",
		"scripts-dir",
		"http":
		return true
	}
	return false
}

func ValidateDependencyTarget(Type string, Target []string) (bool, error) {
	switch Type {
	case "service":
		targetLength := len(Target)
		if targetLength == 1 {
			return true, nil
		} else {
			err := errors.New(`Unexpected number of service "` + strconv.Itoa(targetLength) + `", expected 1 service per dependency definition`)
			return false, err
		}
	case "network":
		networkArgsNumberErrMsg := `Unexpected networks target, expected [host, port], [host:port] or host:port`
		targetLength := len(Target)
		if targetLength == 2 {
			return true, nil
		} else if targetLength == 1 {
			networkArgs := strings.Split(Target[0], ":")
			if len(networkArgs) != 2 {
				return false, errors.New(networkArgsNumberErrMsg)
			}
			host := networkArgs[0]
			if !(ValidateHostname(host) || ValidateIpAddress(host)) {
				return false, errors.New(`Unexpected networks target, invalid host (dns name or ip): "` + host + `"`)
			}
			port := networkArgs[1]
			if !ValidatePort(port) {
				return false, errors.New(`Unexpected networks target, invalid port: "` + port + `"`)
			}
			return true, nil
		} else {
			return false, errors.New(networkArgsNumberErrMsg)
		}
	case "script":
		cmd := Target[0]
		_, lookErr := exec.LookPath(cmd)
		if lookErr != nil {
			return false, lookErr
		}
		return true, nil
	case "scripts-dir":
    targetLength := len(Target)

		if targetLength > 1 {
      return false, fmt.Errorf(`Unexpected scripts-dir target "%v", expected [dir]`, Target)
    }

		dir := Target[0]
    dirStat, err := os.Stat(dir)
    if !os.IsNotExist(err) {
      if err != nil {
        return false, err
      }
      if !dirStat.IsDir() {
        return false, fmt.Errorf(`Target dir %v is not a directory`, dir)
      }
    }
		return true, nil
	case "http":
		targetLength := len(Target)

		if targetLength < 1 {
			return false, fmt.Errorf(`Unexpected http target %v, expected [url], [url, httpCode] or [url, httpCode1RangeStart-httpCode1RangeEnd, !notHttpCode2, httpCode3]`, Target)
		}

		url := Target[0]
		if !ValidateUrl(url) {
			return false, fmt.Errorf(`Unexpected http target, invalid url target: "%v"`, url)
		}

		codes := Target[1:]
		for _, code := range codes {

			if code[0:1] == "!" {
				code = code[1:]
			}

			var codeRange bool
			var codeStart string
			var codeEnd string
			codeSplit := strings.Split(code, "-")
			if len(codeSplit) == 2 {
				codeRange = true
				codeStart = codeSplit[0]
				codeEnd = codeSplit[1]
			}

			httpCodeErrMsg := `Unexpected http target, invalid http code target: "%v"`
			if codeRange {
				if isValidHttpCode := ValidateHttpCode(codeStart); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, codeStart)
				}
				if isValidHttpCode := ValidateHttpCode(codeEnd); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, codeEnd)
				}
			} else {
				if isValidHttpCode := ValidateHttpCode(code); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, code)
				}
			}

		}
		return true, nil

	}
	return false, nil
}
