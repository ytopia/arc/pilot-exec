package dep

import (
	"net"
	"strings"
)

func CheckNetwork(host string, port string) bool {
	_, err := net.Dial("tcp", strings.Join([]string{host, port}, ":"))
	if err == nil {
		return true
	} else {
		return false
	}
}
