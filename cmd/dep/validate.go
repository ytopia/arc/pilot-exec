package dep

import (
	"net/url"
	"regexp"
	"strconv"
)

func ValidateIpAddress(host string) bool {
	validIpAddressRegex, _ := regexp.Compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
	return validIpAddressRegex.MatchString(host)
}
func ValidateHostname(host string) bool {
	validHostnameRegex, _ := regexp.Compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]).)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])$")
	return validHostnameRegex.MatchString(host)
}
func ValidatePort(port string) bool {
	validPort, _ := regexp.Compile("^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$")
	return validPort.MatchString(port)
}

func ValidateHttpCode(code string) bool {
	validHttpCode, _ := regexp.Compile("^([1-5][0-9][0-9])$")
	return validHttpCode.MatchString(code)
}

func ValidateInt(str string) bool {
	if _, err := strconv.Atoi(str); err == nil {
		return true
	}
	return false
}

func ValidateUrl(str string) bool {
	if !((len(str) >= 7 && str[0:7] == "http://") || (len(str) >= 8 && str[0:8] == "http://")) {
		return false
	}
	if _, err := url.ParseRequestURI(str); err == nil {
		return true
	}
	return false
}
