package dep

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
	// "encoding/json"

	"github.com/yosuke-furukawa/json5/encoding/json5"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/config"
	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

func NormalizeDependency(dep interface{}) Dependency {
	var Type string
	var Target []string
	var Retry interface{}
	var RetryTimeout interface{}

	var depType string
	depType = reflect.TypeOf(dep).String()
	if depType == "string" {
		depStr := fmt.Sprintf("%v", dep)
		Target = strings.Split(depStr, " ")
		depType = "[]"
	} else if depType == "[]" {
		Target = dep.([]string)
	} else if depType == "[]interface {}" {
		Target = targetInterfaceToValue(dep)
	}

	if depType == "[]" || depType == "[]interface {}" {
		if len(Target[0]) == 0 {
      err := errors.New(`Unexpected empty value for target`)
      logger.Log.FatalF("%v",err)
    } else if Target[0][0:1] == "/" && Target[0][len(Target[0])-1:] == "/" {
			Type = "scripts-dir"
    } else if Target[0][0:1] == "/" {
      Type = "script"
		} else if ValidateUrl(Target[0]) {
			Type = "http"
		} else if strings.Contains(Target[0], ":") {
			Type = "network"
		} else {
			Type = "service"
		}
	} else if depType == "map[string]interface {}" {
		depMap := dep.(map[string]interface{})
		Type = depMap["type"].(string)
		if !ValidateDependencyType(Type) {
			err := errors.New(`Unexpected value "` + Type + `" of dependency type, expected "service","network", "script", "scripts-dir" or "http"`)
			logger.Log.FatalF("%v",err)
		}

		switch depMap["retry"].(type) {
		case string:
			retry, err := parseRetry(depMap["retry"].(string))
			if err != nil {
				logger.Log.FatalF("%v",err)
			}
			Retry = retry
		case float64:
			Retry = int(depMap["retry"].(float64))
		case bool:
			Retry = depMap["retry"].(bool)
		}

		switch depMap["retry-timeout"].(type) {
		case string:
			retryTimeoutString := depMap["retry-timeout"].(string)
			duration, err := parseRetryTimeout(retryTimeoutString)
			if err != nil {
				logger.Log.FatalF("%v",err)
			}
			RetryTimeout = duration
		case float64:
			RetryTimeout = time.Duration(int(depMap["retry-timeout"].(float64))) * time.Second
		case bool:
			RetryTimeout = depMap["retry-timeout"].(bool)
		}

		targetValue := depMap["target"]
		switch targetValue.(type) {
		case string:
			Target = []string{targetValue.(string)}
		case []string:
			Target = targetValue.([]string)
		case []interface{}:
			Target = targetInterfaceToValue(targetValue)
		default:
			var targetValueType string
			if targetValue == nil {
				targetValueType = "nil"
			} else {
				targetValueType = reflect.TypeOf(targetValue).String()
			}
			err := errors.New(`Unexpected type "` + targetValueType + `" of dependency target, expected string or array of strings`)
			logger.Log.FatalF("%v",err)
		}
	} else {
		err := errors.New(`Unexpected type "` + depType + `" of dependency, expected map, array or string`)
		logger.Log.FatalF("%v",err)
	}

  if ok, err := ValidateDependencyTarget(Type, Target); !ok {
    logger.Log.FatalF("%v",err)
  }

	if Retry == nil {
		retry, err := parseRetry(config.Config.RETRY)
		if err != nil {
			logger.Log.FatalF("%v",err)
		}
		Retry = retry
	}

	if RetryTimeout == nil {
		retryTimeout, err := parseRetryTimeout(config.Config.TIMEOUT)
		if err != nil {
			logger.Log.FatalF("%v",err)
		}
		RetryTimeout = retryTimeout
	}

	var dependency Dependency
	dependency = Dependency{
		Type:         Type,
		Target:       Target,
		Retry:        Retry,
		RetryTimeout: RetryTimeout,
	}

	return dependency
}

func NormalizeDependencies(dependenciesJSON string) []Dependency {
	var dependenciesValues []interface{}
	err := json5.Unmarshal([]byte(dependenciesJSON), &dependenciesValues)
	if err != nil {
		logger.Log.FatalF("%v",err)
	}
	dependencies := []Dependency{}
	for _, dep := range dependenciesValues {
		dependency := NormalizeDependency(dep)
		dependencies = append(dependencies, dependency)
	}
	// fmt.Printf("%v", dependencies)
	return dependencies
}

func targetInterfaceToValue(targetValue interface{}) []string {
	var Target []string
	for _, val := range targetValue.([]interface{}) {
		valType := reflect.TypeOf(val).String()
		if valType != "string" {
			err := errors.New(`Unexpected type "` + valType + `" of dependency target element, expected string`)
			logger.Log.FatalF("%v",err)
		}
		Target = append(Target, val.(string))
	}
	return Target
}

func parseRetry(retryString string) (interface{}, error) {
	var err error
	var retry interface{}
	if retryString == "" {
		retry = nil
	} else if retryString == "1" {
    retry = 1
	} else {
		b, err := strconv.ParseBool(retryString)
		if err == nil {
			retry = b
		} else {
			i, err := strconv.Atoi(retryString)
			if err != nil {
				err = fmt.Errorf(`Unexpected retry format "%v", expected int (e.g: 10) or bool (e.g "true")`, retryString)
			}
			retry = i
		}
	}
	return retry, err
}

func parseRetryTimeout(retryTimeoutString string) (interface{}, error) {
	var err error
	var RetryTimeout interface{}
	if retryTimeoutString == "" {
		RetryTimeout = nil
	} else {
		if ValidateInt(retryTimeoutString) {
			retryTimeoutSeconds, _ := strconv.Atoi(retryTimeoutString)
			RetryTimeout = time.Duration(retryTimeoutSeconds) * time.Second
		} else {
			RetryTimeout, err = time.ParseDuration(retryTimeoutString)
			if err != nil {
				err = fmt.Errorf(`Unexpected retry-timeout format "%v", expected seconds (e.g: 60) or duration (e.g 1h15m30s)`, retryTimeoutString)
			}
		}
	}
	return RetryTimeout, err
}
