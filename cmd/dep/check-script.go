package dep

import (
	"fmt"
	"os"
	"os/exec"
	"log"

	"github.com/kvz/logstreamer"
  goLogger "github.com/apsdehal/go-logger"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

func CheckScript(script []string) bool {


	cmd := exec.Command(script[0], script[1:]...)

  logLevelInt := logger.GetLogLevel()
	if logLevelInt >= goLogger.InfoLevel {
		prefix := fmt.Sprintf(`%s | %s | script %v | `, logger.ModuleName, "INFO", script)
		loggerOut := log.New(os.Stdout, prefix, 0)
		loggerErr := log.New(os.Stderr, prefix, 0)
		logStreamerOut := logstreamer.NewLogstreamer(loggerOut, "", true)
		defer logStreamerOut.Close()
		logStreamerErr := logstreamer.NewLogstreamer(loggerErr, "error", true)
		defer logStreamerErr.Close()
		cmd.Stdout = logStreamerOut
		cmd.Stderr = logStreamerErr
		logStreamerErr.FlushRecord()
	}

	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}
