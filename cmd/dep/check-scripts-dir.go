package dep

import (
	"fmt"
	"log"
	"os"
	"sort"
	"os/exec"
  "path/filepath"

	"github.com/kvz/logstreamer"
  goLogger "github.com/apsdehal/go-logger"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

func CheckScriptsDir(scriptsDir string) bool {

	logLevel := logger.GetLogLevel()


  if _, err := os.Stat(scriptsDir); os.IsNotExist(err) {
    logger.Log.NoticeF(`scripts-dir path "%v" doesn't exists, skipping`, scriptsDir)
    return true
  }

  var scripts []string

  err := filepath.Walk(scriptsDir, func(path string, info os.FileInfo, err error) error {
    if !info.IsDir() {
      scripts = append(scripts, path)
    }
    return nil
  })
  if err != nil {
    logger.Log.FatalF("%v", err)
  }
  sort.Strings(scripts)

  for _, script := range scripts {

    cmd := exec.Command(script)
    if logLevel >= goLogger.InfoLevel {
      prefix := fmt.Sprintf(`%s | %s | script %v | `, logger.ModuleName, "INFO", script)
      loggerOut := log.New(os.Stdout, prefix, 0)
      loggerErr := log.New(os.Stderr, prefix, 0)
      logStreamerOut := logstreamer.NewLogstreamer(loggerOut, "", true)
      defer logStreamerOut.Close()
      logStreamerErr := logstreamer.NewLogstreamer(loggerErr, "error", true)
      defer logStreamerErr.Close()
      logStreamerErr.FlushRecord()
      cmd.Stdout = logStreamerOut
      cmd.Stderr = logStreamerErr
    }
    if err := cmd.Run(); err != nil {
      return false
    }

  }

	return true
}
