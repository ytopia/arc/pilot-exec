package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"encoding/json"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
  "github.com/yosuke-furukawa/json5/encoding/json5"

	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/cmdOptions"
	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/config"
	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/dep"
	"gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
)

var rootCmd = &cobra.Command{
	Use:   "pilot-exec",
	Short: "Dependencies manager for container autopilot pattern",
	Long:  "Wait for a list of dependencies: service (consul), a network (host:port), http (url code), script or scripts-dir to run a program or exit",
  Args: cobra.ArbitraryArgs,
	Run: func(cobraCmd *cobra.Command, argv []string) {

    var err error
    JSON := config.Config.DEPENDENCIES
  	var dependenciesInterface []interface{}
  	if err := json5.Unmarshal([]byte(JSON), &dependenciesInterface); err != nil {
  		fmt.Println(err)
  		logger.Log.FatalF("Unable to parse JSON for config key %s", "DEPENDENCIES")
  	}
    jsonByte, err := json.Marshal(dependenciesInterface)
  	if err != nil {
  		logger.Log.FatalF("%v",err)
  	}
  	dependencies := string(jsonByte)

		_, err = dep.On(dependencies)
		if err != nil {
      os.Exit(1)
		}

		if len(argv) > 0 {

      binary, lookErr := exec.LookPath(argv[0])
  		if lookErr != nil {
        log.Fatal(lookErr)
  		}

      logger.Log.Info("--")
      logger.Log.InfoF("%v", strings.Join(argv, " "))

      env := os.Environ()
      execErr := syscall.Exec(binary, argv, env)
      if execErr != nil {
        log.Fatal(execErr)
      }

		}
	},
}


func init() {
	cobra.OnInitialize(config.Init)

	defaultConfig := os.Getenv(config.PrefixEnv("CONFIG"))
	if defaultConfig == "" {
		defaultConfig = "./.pilot-exec.yml"
	}
	rootCmd.PersistentFlags().StringVarP(&config.File, "config", "c", defaultConfig, "pilot-exec config file")
	viper.BindPFlag("CONFIG", rootCmd.PersistentFlags().Lookup("config"))

	cmdOptions.AddFlagConsulHost(rootCmd)
	cmdOptions.AddFlagConsulPort(rootCmd)
	cmdOptions.AddFlagConsulHttps(rootCmd)
	cmdOptions.AddFlagLogLevel(rootCmd)
	cmdOptions.AddFlagLogColor(rootCmd)

	cmdOptions.AddFlagDependencies(rootCmd)
	cmdOptions.AddFlagRetry(rootCmd)
	cmdOptions.AddFlagTimeout(rootCmd)
}


func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
