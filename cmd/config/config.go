package config

import (
  "log"
  "strings"

  "github.com/spf13/viper"

  "gitlab.com/youtopia.earth/ops/pilot-exec/cmd/logger"
  "gitlab.com/youtopia.earth/ops/pilot-exec/cmd/utils/fs"
)

var EnvPrefix = "PILOT_EXEC"
func PrefixEnv (key string) string{
  return EnvPrefix+"_"+key
}

type ConfigMap struct {
  CONSUL_HOST string
  CONSUL_PORT string
  CONSUL_HTTPS bool
  DEPENDENCIES string
  RETRY string
  TIMEOUT string
  LOG_LEVEL string
  LOG_COLOR string
}

var File string
var Config ConfigMap

func Init() {
	viper.AutomaticEnv()
	viper.AllowEmptyEnv(false)
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)
  viper.SetEnvPrefix(EnvPrefix)

	if File != "" {
		if ok, _ := fs.Exists(File); ok {
			viper.SetConfigFile(File)

			if err := viper.ReadInConfig(); err != nil {
				if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
					log.Fatal(err)
				}
			}
		}
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			log.Fatal(err)
		}
	}

  if err := viper.Unmarshal(&Config); err != nil {
    log.Fatal("Unable to unmarshal config")
  }

  logger.Load()

}
