package main

import (
  "gitlab.com/youtopia.earth/ops/pilot-exec/cmd"
)

func main() {
  cmd.Execute()
}