PROJECT_NAME := "pilot-exec"
PKG := "gitlab.com/youtopia.earth/ops/$(PROJECT_NAME)"

all: build targz


build:
	@go build -o bin/pilot-exec -i -v $(PKG)

targz:
	cd bin && tar -cvzf pilot-exec.tar.gz pilot-exec

fmt:
	@gofmt -w cmd


vendor:
	@go mod vendor



help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
