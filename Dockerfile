ARG GOLANG_VERSION=latest
FROM golang:$GOLANG_VERSION as builder

ENV GOFLAGS=-mod=vendor
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o=pilot-exec .

FROM scratch
COPY --from=builder /app/pilot-exec /

CMD ["/pilot-exec"]