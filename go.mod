module gitlab.com/youtopia.earth/ops/pilot-exec

go 1.12

require (
	github.com/apsdehal/go-logger v0.0.0-20190515212710-b0d6ccfee0e6
	github.com/kvz/logstreamer v0.0.0-20150507115422-a635b98146f0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/yosuke-furukawa/json5 v0.1.1
)
